import React from "react"
import { Col, Container, Row } from "reactstrap"
import logo from "../assets/img/logo/logo-dummy@2x.png"

export default function Head() {
  return(
    <Container className="">
      <Row>
        <Col>
          <img src={logo} width="140px" alt="logo"/>
        </Col>
      </Row>
      <Row className="mt-3">
        <Col xl="2" lg="2" md="2" className="mr-2">
          <p> Filter</p>
          <hr style={{borderTop: "1px solid rgba(0, 0, 0, 0.1)", marginTop:"1rem", marginBottom:"1rem", border:"0"}} />
        </Col>
        <Col xl="10" lg="10" md="10">
            <p>Product List</p>
          <hr style={{borderTop: "1px solid rgba(0, 0, 0, 0.1)", marginTop:"1rem", marginBottom:"1rem", border:"0"}} />
        </Col>
      </Row>
    </Container>
  )
}
