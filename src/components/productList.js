import React from "react";
import {
  Button,
  Card,
  CardBody,
  CardImg,
  CardSubtitle,
  CardText,
  CardTitle,
  Col,
  Row,
} from "reactstrap";
import { Link } from "react-router-dom";
import ReactStars from "react-rating-stars-component";
import { AiTwotoneHeart, AiOutlineHeart } from "react-icons/ai";

export default function ProductList(props) {
  // const ratingChanged = (newRating) => {
  //   console.log(newRating);
  // };

  return (
    <Col lg="4" className="mb-5">
      <Card id={props.id} className="mr-4">
        <Row>
          {props.stock === 0 ? (
            <>
              <Col md="auto" lg="auto">
                <CardText className="text-danger m-2">Sold Out</CardText>
              </Col>
              {props.isNew === 1 ? (
                <Col md="auto" lg="auto">
                  <CardText className="text-info m-2"> New </CardText>
                </Col>
              ) : null}
              {props.rating >= 4 && props.numOfReviews > 25 ? (
                <Col md="auto" lg="auto">
                  <CardText className="text-info m-2"> Best Seller </CardText>
                </Col>
              ) : null}
              {props.rating >= 4 &&
              props.numOfReviews > 25 &&
              props.isNew === 1 ? (
                <Col md="auto" lg="auto">
                  <CardText className="text-info m-2"> Hot Seller </CardText>
                </Col>
              ) : null}
            </>
          ) : props.stock < 5 ? (
            <>
              <Col md="auto" lg="auto">
                <CardText className="text-danger m-2">Stock 5 Kurang</CardText>
              </Col>
              {props.isNew === 1 ? (
                <Col md="auto" lg="auto">
                  <CardText className="text-info m-2"> New </CardText>
                </Col>
              ) : null}
              {props.rating >= 4 && props.numOfReviews > 25 ? (
                <Col md="auto" lg="auto">
                  <CardText className="text-info m-2"> Best Seller </CardText>
                </Col>
              ) : null}
              {props.rating >= 4 &&
              props.numOfReviews > 25 &&
              props.isNew === 1 ? (
                <Col md="auto" lg="auto">
                  <CardText className="text-info m-2"> Hot Seller </CardText>
                </Col>
              ) : null}
            </>
          ) : (
            <>
              <Col md="auto" lg="auto">
                <CardText className="text-primary m-2">In Stock</CardText>
              </Col>
              {props.isNew === 1 ? (
                <Col md="auto" lg="auto">
                  <CardText className="text-info m-2"> New </CardText>
                </Col>
              ) : null}
              {props.rating >= 4 && props.numOfReviews > 25 ? (
                <Col md="auto" lg="auto">
                  <CardText className="text-info m-2"> Best Seller </CardText>
                </Col>
              ) : null}
              {props.rating >= 4 &&
              props.numOfReviews > 25 &&
              props.isNew === 1 ? (
                <Col md="auto" lg="auto">
                  <CardText className="text-info m-2"> Hot Seller </CardText>
                </Col>
              ) : null}
            </>
          )}
        </Row>
        <Link to={`/product/` + props.id} style={{ textDecoration: "none" }}>
          <Card className="p-5">
            <CardImg src={props.img} width="70%" />
          </Card>
        </Link>
        <CardBody>
          <CardTitle className="m-3 text-dark">{props.name}</CardTitle>
          <CardSubtitle className="text-success" style={{ marginLeft: "15px" }}>
            {props.points} poins
          </CardSubtitle>
          <Row style={{ marginLeft: "11px" }}>
            <Col>
              <ReactStars
                count={5}
                // onChange={ratingChanged}
                size={24}
                value={props.rating}
                edit={false}
                activeColor="#ffd700"
              />
            </Col>
            <Col>
              <p>{props.numOfReviews} reviews</p>
            </Col>
            <Col>
              <Button outline color="secondary" block>
                <AiOutlineHeart />
              </Button>
            </Col>
          </Row>
        </CardBody>
      </Card>
    </Col>
  );
}
