import React from "react";
import { Col, Container, Row } from "reactstrap";
import logo from "../assets/img/logo/logo-dummy@2x.png";


export default function HeadProduct() {
  return (
    <Container className="">
      <Row>
        <Col>
          <img src={logo} width="140px" alt="logo" />
        </Col>
      </Row>
    </Container>
  );
}
