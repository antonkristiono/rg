// import logo from './logo.svg';
// import './App.css';
import React from "react";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import Product from "./pages/product";
import Home from "./pages/home";

function App() {
  return (
    <>
      <Router>
        <Switch>
          <Route exact path="/" component={Home} />
          <Route path="/product/:id" component={Product} />
        </Switch>
      </Router>
    </>
  );
}

export default App;
