import React, { useState, useEffect } from "react";
import Home2 from "../layout/home2";
import { Col, Container, Row, Card, CardBody, Input } from "reactstrap";
import styled from "styled-components";
import { useRecoilState } from "recoil";
// import { getAllData} from "./lib/axios"
import { allGifts } from "../lib/recoil";
import axios from "axios";
import ProductList from "../components/productList";
import { Switch, Route } from "react-router-dom";
import Product from "../pages/product";

const TextBox = styled.div`
  font-size: 12px;
  text-align: left;
`;

export default function Home() {
  // const [coba,setcoba] = useRecoilState(allGifts)
  const [products, setProducts] = useRecoilState(allGifts);

  useEffect(() => {
    axios
      .get(
        "https://recruitment.dev.rollingglory.com/api/v2/gifts?page[number]=1&page[size]=6"
      )
      .then((response) => {
        console.log(response.data);
        console.log(response.data.data);
        let prods = response.data.data;
        setProducts(prods);
        console.log(products);
      });
  }, []);

  return (
    <>
      <Home2 />
      <Container>
        <Row>
          <Col xl="2" lg="2" md="2" className="mr-2">
            <Card>
              <CardBody>
                <TextBox>
                  {" "}
                  Rating 4 ke Atas <Input type="checkbox" id="checkbox4" />
                </TextBox>
                <br />
                <TextBox>
                  {" "}
                  Rating 5 ke Atas <Input type="checkbox" id="checkbox5" />{" "}
                </TextBox>
                {/* <Link to="/product"> */}
                {/* <Button onClick={handleClick}>a</Button> */}
                {/* </Link> */}
                <Switch>
                  <Route path="/product">
                    <Product />
                  </Route>
                </Switch>
              </CardBody>
            </Card>
          </Col>
          <Col xl="10" lg="10" md="10">
            <Row>
              {products.map((produk) => (
                <ProductList
                  key={produk.attributes.id}
                  id={produk.attributes.id}
                  img={produk.attributes.images}
                  name={produk.attributes.name}
                  points={produk.attributes.points}
                  stock={produk.attributes.stock}
                  isNew={produk.attributes.isNew}
                  isWishlist={produk.attributes.isWishlist}
                  numOfReviews={produk.attributes.numOfReviews}
                  rating={produk.attributes.rating}
                />
              ))}
            </Row>
          </Col>
        </Row>
      </Container>
    </>
  );
}
