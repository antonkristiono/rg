import React, { useState, useEffect } from "react";
import HeadProduct from "../components/headProduct";
import {
  Row,
  Col,
  Container,
  Card,
  CardImg,
  CardBody,
  CardTitle,
  CardSubtitle,
  CardText,
} from "reactstrap";
import { Link, useParams } from "react-router-dom";
import axios from "axios";
import ReactStars from "react-rating-stars-component";

export default function Product() {
  let { id } = useParams();
  const [prod, setProd] = useState({
    name: null,
    description: null,
    images: null,
    info: null,
    stock: null,
    rating: null,
    points: null,
    numOfReviews: null,
    isWhislist: null,
    isNew: null,
  });

  useEffect(() => {
    axios
      .get("https://recruitment.dev.rollingglory.com/api/v2/gifts/" + id)
      .then((response) => {
        // console.log(response.data);
        // console.log(response.data.data);
        let prods = response.data.data.attributes;
        setProd(prods);
        console.log(prods);
      });
  }, []);

  return (
    <>
      <HeadProduct />
      <Container className="mt-3 ml-3">
        <Row>
          <Col xl="4" lg="4" md="4" className="mr-2">
            <Link to="/">
              <p>
                List Product {">"} {prod.name}
              </p>
            </Link>
          </Col>
        </Row>
      </Container>
      <Container>
        <Row>
          <Col lg="6" md="6">
            <Card>
              <CardImg src={prod.images} width="120px" className="p-5" />
            </Card>
          </Col>
          <Col lg="6" md="6">
            <Card>
              <CardBody className="p-5">
                <CardTitle>
                  <h2>{prod.name}</h2>
                </CardTitle>
                <Row>
                  <Col>
                    <ReactStars
                      count={5}
                      size={24}
                      value={prod.rating}
                      edit={false}
                      activeColor="#ffd700"
                    />
                  </Col>
                  <Col>
                    <p className="mt-2">{prod.numOfReviews} reviews</p>
                  </Col>
                </Row>
                <CardText>
                  {" "}
                  <h3>{prod.points} poins</h3>
                </CardText>
                <div
                  dangerouslySetInnerHTML={{ __html: prod.description }}
                ></div>
              </CardBody>
            </Card>
          </Col>
        </Row>
      </Container>
    </>
  );
}
