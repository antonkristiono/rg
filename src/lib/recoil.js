import { atom } from "recoil";

const allGifts = atom({
  key: "allGifts",
  default: [],
})

export { allGifts }
