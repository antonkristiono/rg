import axios from "axios"

export async function getAllData(){
  try {
  const response =  await axios.get('https://recruitment.dev.rollingglory.com/api/v2/gifts?page[number]=1&page[size]=6')
    return response
  } catch (error) {
    throw new Error (
      'Not get Axios'
    )
  }
}
